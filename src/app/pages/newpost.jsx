import React, { useState } from 'react';
import { axios_create } from '../../helper/axios_create';

const UseHandler = (name) => {
    const [ value, setterFn ] = useState();
    const changeHandler = (e) => {
        setterFn(  e.target.value );
    }
    return {
        value: value,
        handler: changeHandler
    }
}

const NewPost = () => {
    const title = UseHandler();
    const body = UseHandler();

    const onClickHandler = (e) => {
        e.preventDefault();
        console.log(title.value, body.value);
        axios_create(title.value, body.value);
    }

    return(
        <form className="container w-50">
            <h3>You are going to create new post</h3>
            <label className="row">
                <span>Title:</span>
                <input 
                    className="form-control"
                    onChange={title.handler}
                    value={title.value}
                />
            </label>
            <label className="row">
                <span>Body:</span>
                <textarea 
                    className="form-control"
                    onChange={body.handler}
                    value={body.value}
                />
            </label>
            <button type="submit" className="btn btn-primary btn-lg btn-block" onClick={onClickHandler}>Publish</button>
        </form>
    )
};

export default NewPost;