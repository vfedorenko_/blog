import React, { useState } from 'react';
import { axios_comm } from '../../helper/axios_comm';

const UseHandler = (name) => {
    const [ value, setterFn ] = useState();
    const changeHandler = (e) => {
        setterFn(  e.target.value );
    }
    return {
        value: value,
        handler: changeHandler
    }
}

const Comment = ( props ) => {
    const body = UseHandler();

    const publComm = (e) => {
        // e.preventDefault();
        console.log(props.id, body.value);
        axios_comm(props.id, body.value);
    }

    return(
        <>
            <label className="w-100">
                <span>Write your comment:</span>
                <textarea 
                    className="form-control"
                    name="body"
                    onChange={body.handler}
                    value={body.value}
                />
            </label>
            <button className="btn btn-outline-primary btn-block" onClick={publComm}>Publish comment</button>
        </>
    )
}

export default Comment;