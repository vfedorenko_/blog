import { combineReducers } from 'redux';

import postsReducer from './posts';
import postReducer from './post';

const reducer = combineReducers({
    postsReducer,
    postReducer,
});

export default reducer;