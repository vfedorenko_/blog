import axios from 'axios';

// Base url for posts requests

export const axios_posts = axios.create({
    baseURL: 'https://bloggy-api.herokuapp.com/posts',
});