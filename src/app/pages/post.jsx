import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Comment from './comment';

import { getPostPromise } from '../../actions';

class Post extends React.Component {
    componentDidMount = () => {
        const { match, getPost} = this.props;
        getPost(match.params.id);
    }

    
    render = () => {
        let { loaded, data } = this.props.post;
        const { id } = this.props.match.params;

        return(
            <>
                {
                    loaded 
                    ? <div className="card w-75 m-auto">
                        
                        <h1 className="card-title">{data.title}</h1>
                        <p className="card-text">{data.body}</p>
                        <Link className="btn btn-outline-primary btn-lg btn-block" to={"/edit/" + id}>Redact post</Link>
                        
                        <div>
                            <Comment id={id}/>
                            <h3 className="card-subtitle">comments</h3>
                            <ol className="list-group list-group-flush">
                                {
                                    data.comments.map( comm => {
                                        return(
                                            <li className="list-group-item">{comm.body}</li>
                                        )
                                    } )
                                }
                                
                            </ol>
                        </div>

                    </div>
                    : <h2>Loading...</h2>
                }
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    post: state.postReducer,
});

const mapDispatchToProps = ( dispatch ) => ({
    getPost: ( id ) => {
        dispatch( getPostPromise( id ) );
    },
})

export default connect( mapStateToProps, mapDispatchToProps )( Post );