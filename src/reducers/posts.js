import {
    GET_POSTS_REQ,
    GET_POSTS_RES,
    GET_POSTS_ERROR
} from '../actions';
import { InitialState } from './initialState';

// State for a list of all posts

const postsReducer = ( state = InitialState, action) => {
    switch( action.type ){

        case GET_POSTS_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_POSTS_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        case GET_POSTS_ERROR:
            return{
                ...state,
                loading: false,
                loaded: false,
                errors: action.error
            }

        default:
            return state;
    }
}

export default postsReducer;