import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { getPostPromise } from '../../actions';
import { axios_put } from '../../helper/axios_put';
import { axios_del } from '../../helper/axios_del';

class EditPost extends React.Component {
    state = {
        id: this.props.match.params.id,
        title: this.props.post.data.title,
        body: this.props.post.data.body,
        edited: false
    }

    componentDidMount = () => {
        const { getPost} = this.props;
        getPost(this.state.id);
    }

    componentDidUpdate = (prevProps, prevState) => {
        const { loaded, data } = this.props.post;
        if (prevProps.post.loaded !== loaded) {
            this.setState({
                title: data.title,
                body: data.body
            })
        }
    }
    
    onClickHandler = (e) => {
        // e.preventDefault();
        console.log(this.state);
        axios_put(this.state);
        this.setState({
            edited: true
        })
    }

    handler = (e) => {
        console.log(e.target.name, e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    deletePost = ( e ) => {
        // e.preventDefault();
        console.log('deleted))');
        axios_del(this.state.id);
        this.setState({
            edited: true
        })
    }

    render = () => {
        const { title, body, edited } = this.state;
        const { handler } = this;
        const { onClickHandler, deletePost } = this;

        return(
            <form className="container w-50">
                <h3>You are going to redact the post</h3>
                <label className="row">
                    <span>Title:</span>
                    <input 
                        className="form-control"
                        name="title"
                        onChange={handler}
                        value={title}
                    />
                </label>
                <label className="row">
                    <span>Body:</span>
                    <textarea 
                        className="form-control"
                        name="body"
                        onChange={handler}
                        value={body}
                    />
                </label>
                <button type="submit" className="btn btn-primary btn-lg btn-block" onClick={onClickHandler}>Publish</button>
                <button className="btn btn-danger btn-lg btn-block" onClick={deletePost}>Remove the Post</button>
                {
                    edited && <Redirect to="/"/>
                }
            </form>
        )
    }
};

const mapStateToProps = (state) => ({
    post: state.postReducer,
});

const mapDispatchToProps = ( dispatch ) => ({
    getPost: ( id ) => {
        dispatch( getPostPromise( id ) );
    },
})

export default connect( mapStateToProps, mapDispatchToProps )( EditPost );