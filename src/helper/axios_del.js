import { axios_posts } from './axios_posts';

// Remove a post request with id of the post. Type of request DELETE. 

export const axios_del = async ( id ) => {
    console.log('delete', id);
    await axios_posts.delete( id )
        .then( response => console.log(response) )
        .catch( error => {
            if (error.response) {
                console.log(error.response);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        } )
} 
