import { axios_comment } from './axios_comments';

// Request for create new comment for post id with text body

export const axios_comm = async ( id, body ) => {
    console.log(id, body);
    await axios_comment.post( '', {
            "postId": id, 
            "body": body
        } )
        .then( response => console.log(response) )
        .catch( error => {
            if (error.response) {
                console.log(error.response);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        } )
} 