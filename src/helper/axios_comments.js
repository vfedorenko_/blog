import axios from 'axios';

// Base url for comment requests

export const axios_comment = axios.create({
    baseURL: 'https://bloggy-api.herokuapp.com/comments',
});