import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { getPostsPromise } from '../../actions';

class Posts extends React.Component {
    componentDidMount = () => {
        if (!this.props.list.loaded){
            this.props.getAllPosts();
        }
    }

    render = () => {
        let { loaded, data } = this.props.list;

        return(
            <>
                <h1>All posts</h1>
                {
                    loaded 
                    ? data.map( item => {
                        return(
                            <Link key={item.id} to={'/post/' + item.id}>
                                <div className="card">
                                    <h3 className="card-title">{item.title}</h3>
                                    <p className="card-text">{item.body}</p>
                                </div>
                            </Link>
                        )
                    } )
                    : <h2>Loading...</h2>
                }
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    list: state.postsReducer,
});

const mapDispatchToProps = ( dispatch ) => ({
    getAllPosts: () => {
        dispatch( getPostsPromise() );
    },
})

export default connect( mapStateToProps, mapDispatchToProps )( Posts );