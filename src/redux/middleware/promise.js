import { PROMISE } from '../../actions';

console.log (PROMISE)

const promise = (store) => (next) => (action) => {
    console.log(action)
    if (action.type !== PROMISE) {
        return next(action);
    } else {
        const [startAct, successAct, failureReq] = action.actions;
        store.dispatch({
            type: startAct
        })
        return action.promise.then( res => store.dispatch({
            type: successAct,
            payload: res.data
        })).catch( error => store.dispatch({ 
            type: failureReq,
            error: error
        }) )
    }
}

export default promise;