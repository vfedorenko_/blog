import { axios_posts } from '../helper/axios_posts';
import { PROMISE } from './index';

export const GET_POSTS_REQ = 'GET_POSTS_REQ';
export const GET_POSTS_RES = 'GET_POSTS_RES';
export const GET_POSTS_ERROR = 'GET_POSTS_ERROR';

// Actions for postsReducer in ../reducers/posts.js

export const getPostsPromise = () => ( dispatch ) => {
    console.log( 'in actions');
    dispatch({
        type: PROMISE,
        actions: [ GET_POSTS_REQ, GET_POSTS_RES, GET_POSTS_ERROR ],
        promise: axios_posts.get()
    });
}