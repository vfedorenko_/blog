import { axios_posts } from './axios_posts';

// Create new post request with title and body. Type of request POST. 

export const axios_create = async ( title, body ) => {
    console.log(title, body);
    await axios_posts.post( '', {
            "title": title, 
            "body": body
        } )
        .then( response => console.log(response) )
        .catch( error => {
            if (error.response) {
                console.log(error.response);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        } )
} 
