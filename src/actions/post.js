import { axios_posts } from '../helper/axios_posts';
import { PROMISE } from './index';

export const GET_POST_REQ = 'GET_POST_REQ';
export const GET_POST_RES = 'GET_POST_RES';
export const GET_POST_ERROR = 'GET_POST_ERROR';

// Actions for postReducer in ../reducers/post.js

export const getPostPromise = ( id ) => ( dispatch ) => {
    console.log( 'in post actions');
    dispatch({
        type: PROMISE,
        actions: [ GET_POST_REQ, GET_POST_RES, GET_POST_ERROR ],
        promise: axios_posts.get( id, {
                    params: {
                        '_embed': 'comments'
                    }
                } )
    });
}