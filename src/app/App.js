import React from 'react';
import { NavLink, Route, Switch } from 'react-router-dom';

import { ROUTES } from './routes';

import './App.css';

const App = () => {

		return (
			<div className="App">
				<header className="App-header">
					<nav>
						<NavLink exact to='/'>Main page</NavLink>
						<NavLink exact to='/create'>Create a Post</NavLink>
					</nav>
				</header>
				<Switch>
					{
						ROUTES.map( (route, index) => {
							return (<Route 
								key={index}
								{ ...route }
							/>)
						} )
					}
				</Switch>
			</div>
		);

}

export default App;
