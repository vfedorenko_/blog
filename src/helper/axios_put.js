import { axios_posts } from './axios_posts';

// Update a post request with id, title and body. Type of request PUT. 
// URL for request is base url plus id

export const axios_put = async ( {title, body, id} ) => {
    console.log(title, body);
    await axios_posts.put( id, {
            "title": title, 
            "body": body
        } )
        .then( response => console.log(response) )
        .catch( error => {
            if (error.response) {
                console.log(error.response);
            } else if (error.request) {
                console.log(error.request);
            } else {
                console.log('Error', error.message);
            }
            console.log(error.config);
        } )
} 
