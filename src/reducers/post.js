import {
    GET_POST_REQ,
    GET_POST_RES,
    GET_POST_ERROR
} from '../actions';
import { InitialState } from './initialState';

// State for a single current post

const postReducer = ( state = InitialState, action) => {
    switch( action.type ){

        case GET_POST_REQ: 
            return {
                ...state,
                loaded: false,
                loading: true
            }

        case GET_POST_RES:
            return{
                ...state,
                loaded: true,
                loading: false,
                data: action.payload
            }

        case GET_POST_ERROR:
            return{
                ...state,
                loading: false,
                loaded: false,
                errors: action.error
            }

        default:
            return state;
    }
}

export default postReducer;