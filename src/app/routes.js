import NotFound from './pages/notfound';
import Posts from './pages/posts';
import NewPost from './pages/newpost';
import Post from './pages/post';
import EditPost from './pages/EditPost';

export const ROUTES = [
    {
        path: '/',
        component: Posts,
        exact: true
    },
    {
        path: '/create',
        component: NewPost,
        exact: true
    },
    {
        path: '/post/:id',
        component: Post,
        exact: true
    },
    {
        path: '/edit/:id',
        component: EditPost,
        exact: true
    },
    {
        component: NotFound
    }
]

export default ROUTES;